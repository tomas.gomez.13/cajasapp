import React, { Component } from 'react';
import { Panel, Form, FormGroup, FormControl, Button } from 'react-bootstrap';
import axios from 'axios';
var apiBaseUrl = "";

const divStyle = {
  display: 'flex',
  height: 200,
  alignItems: 'center',
  marginTop: -100,
  border: '1px solid black',
  borderRadius:10,
};

const panelStyle = {
  backgroundColor: '#1a1a1a',
  border: 0,
  paddingLeft: 20,
  paddingRight: 20,
  width: 300,
};

const buttonStyle = {
  marginBottom: 0
};

class LoginForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      datos: [],
    };
  }




  handleFormSubmit(e) {
    e.preventDefault();
    alert(document.getElementById('formRut').value);
    apiBaseUrl="https://0558faf5-fb13-4286-a693-7a78376f30cf.mock.pstmn.io/login?rut="+document.getElementById('formRut').value+"&Work_ID=$2a$12$nv9iV5/UNuV4Mdj1Jf8zfuUraqboSRtSQqCmtOc4F7rdwmOb9IzNu";
  axios.get(apiBaseUrl)
  .then(function (response) {
    const { datos }= response;
   // console.log(datos);
    //alert(response.data.rut);
    
    if (response.data.rut!=document.getElementById('formRut').value)
    {
      alert ('Rut Incorrecto o No hay Datos con este Rut');
      document.getElementById('formRut').focus();
      return false;
    }
    //console.log(response);
   // console.log(response.data.name);
   // console.log(response.data.checkouts[0].id)
    //alert(response.data.checkouts[0].id);
    //console.log("Exitoso!");
    window.location = "/DashBoard?"+'usuario='+response.data.name;
  })
  .catch(function (error) {
      console.log(error);
  });
   
  //  this.props.history.push("/DashBoard");
    
  }

  render() {
    return (
      <div style={divStyle}>
        <Panel style={panelStyle}>
          <Form horizontal className="LoginForm" id="loginForm">
            <FormGroup controlId="formRut">
              <FormControl type="text" placeholder="Ingrese Rut" />
            </FormGroup>
            <FormGroup controlId="formPassword">
              <FormControl type="password" placeholder="Ingrese Password" />
            </FormGroup>
            <FormGroup style={buttonStyle} controlId="formSubmit">
              <Button bsStyle="primary" type="submit" onClick={this.handleFormSubmit}>
                Login de Usuario
              </Button>
            </FormGroup>
          </Form>
        </Panel>
      </div>
    )
  }
}

export default LoginForm;
