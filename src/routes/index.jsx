import Dashboard from "layouts/Dashboard/Dashboard.jsx";
import Login from "../components/Login/Login.js";

var indexRoutes = [{ path: "/Login", name: "Login", component: Login },{ path: "/", name: "Home", component: Dashboard }];

export default indexRoutes;
