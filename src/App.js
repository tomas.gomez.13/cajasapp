import React, { Component } from 'react';
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";


import "bootstrap/dist/css/bootstrap.css";
//import "assets/scss/paper-dashboard.scss";
import "assets/demo/demo.css";
import "assets/scss/paper-dashboard.css";



import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import Dashboard from "./layouts/Dashboard/Dashboard.jsx";
import LoginPage from './components/LoginPage/LoginPage';
import './App.css';

//const Dashboard = () => (
 // <Dashboard />
//);

const Login = () => (
  <LoginPage />
);

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Route exact path="/Dashboard" component={Dashboard} />
          <Route path="/login" component={Login} />
        </div>
      </Router>
    );
  }
}

export default App;
