import React, { Component } from 'react';

class Products extends Component{
	_remove(){
		if(this.props.onRemove)
			this.props.onRemove();
	}
	render(){
		return (
			<li>
				<div className="icon">
					<img src={require(`../Components/images/punto.png`)}   />
				</div>
				
				<div className="price">
					<h3>{this.props.prod}</h3>
				</div>
				<div className="price">
					<h3>{this.props.precio}</h3>
				</div>
				
			</li>
		)
	}
}


export default Products;