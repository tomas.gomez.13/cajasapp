import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Col,
  Table
} from "reactstrap";
// react plugin used to create charts
import { Line, Pie } from "react-chartjs-2";
// function that returns a color based on an interval of numbers

import Stats from "components/Stats/Stats.jsx";
import { NavigationMoreHoriz, NavigationArrowBack } from "material-ui/svg-icons";
import { normalize } from "path";
import {
 
  dashboardEmailStatisticsChart,
  dashboardNASDAQChart
} from "variables/charts.jsx";
import axios from 'axios';
import { SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS } from "constants";
import Products from 'views/Components/products';
import Button from 'views/Components/Button';
import _ from 'lodash';
import 'views/Components/products.css';
import localLaundryService from "material-ui/svg-icons/maps/local-laundry-service";
var apiBaseUrl = "https://0558faf5-fb13-4286-a693-7a78376f30cf.mock.pstmn.io/login?rut=18.782.666-6&Work_ID=$2a$12$nv9iV5/UNuV4Mdj1Jf8zfuUraqboSRtSQqCmtOc4F7rdwmOb9IzNu";
var apiPool = "https://a3d0e1b8-0b5f-4963-85e4-a9b093404400.mock.pstmn.io/pool?pool=%22Parque%20Arauco/Deportes%22";
var apiDetalle = "https://da4c50ab-f60f-4ab9-9f14-b06a6b57ccf7.mock.pstmn.io/checkout?rut="+"18.782.666-6"+"&checkouts="+"[caja1,caja5,caja6]";
var nroco=0;
class Dashboard extends React.Component {
  
  state = {
    pause: true,
   
   
  }
 
  componentDidMount() {
    this.setState({
      pause: this.props,
      
    })
  
  }

  handleChange(event) {
    this.setState({title: event.target.value})
  }
  handleKeyUp(event) {
    if (event.keyCode == 13) return this.sendData()
  }

 
constructor(props){
  super(props);
  var top;
  this.handleAuthorChange = this.handleAuthorChange.bind(this);
   this.state = {
    items:[ { "id": "Parque Arauco/Deportes",
    "checkouts": [{ "id": "caja1",
                     "store": "Parque Arauco",
                     "daySales": "540000",
                     "costumers": "34",
                     "products": "104" },
                   {"id": "caja5",
                    "store": "Parque Arauco",
                    "daySales": "276000", 
                    "costumers": "15", 
                    "products": "54" }, 
                   { "id": "caja6", 
                     "store": "Parque Arauco", 
                     "daySales": "1840000", 
                     "costumers": "92",
                     "products": "409" } ], 
    },
  ],
    

  prod: [{ "response": [ { "id": "caja1", "state": "escaneando producto", 
	"products": [ { "SKU": "2094850944" }, { "SKU": "2049984729" },{ "SKU": "3904994049" } ], 
	"sale": "23990", "last_sale": "43990" }, 
	{ "id": "caja5", "state": "libre", "products": [{"SKU": "2111111111"}], "sale": "0", "last_sale": "18990" }, 
  { "id": "caja6", "state": "libre", "products": [{ "SKU": "3333333333" }], "sale": "0", "last_sale": "65990" } ]
 },
],
   
  item:[],
  prodto:[],

}

var prodtodo={datos:[{cod:''}]};







  this.onFormSubmit = this.onFormSubmit.bind(this)
  //var item = new Array();
  this.state.item.push(this.state.items);
  this.state.item.push(this.state.prod);
  new Object().toString()
// "[object Object]"


// Uncaught SyntaxError: Unexpected token o in JSON at position 1


  
  for (var i=0; i<this.state.prod[0].response.length; i++) {
    var hotel = this.state.prod[0].response[i]; // hasta aquí tienes cada hotel
    for (var detalle in hotel.products) {
    
     
      for (var dato in hotel.products[detalle]) {
       //  console.log (hotel.products[detalle][dato]); // Y aquí exploras cada dato del viaje
        
       this.state.prodto.push({$i:[hotel.products[detalle][dato]]});
       // obj['datos'].push({"nombre":hotel.products[detalle][dato]});
        //prodto.0=hotel.products[detalle][dato];
      }
     
    
    }
    }
    //this.state.items.push(this.state.items);
 // this.state.items.push(this.state.prodto);

    //fin for

    function NumberList(props) {
      const numbers = props.prod;
      const listItems = this.state.prod.map((number) =>
        <li key={number.toString()}>
          {number}
        </li>
      );
      return (
        <ul>{listItems}</ul>
      );
    }
  
   
  }
 
  handleAuthorChange = (event) => {
    const {name: fieldName, value} = event.target;
    this.setState({
      [fieldName]: value
    });
  };

  
  ver(v){
   
    for (var i=v; i<this.state.prod[0].response.length; i++) {
      var hotel = this.state.prod[0].response[i]; // hasta aquí tienes cada hotel
      for (var detalle in hotel.products) {
      
       
        for (var dato in hotel.products[detalle]) {
       //    console.log (hotel.products[detalle][dato]); // Y aquí exploras cada dato del viaje
          
            
     
        }
       
      
      }
      }
  }
 /*  onFormSubmit(event) {
    event.preventDefault(); //What is 'preventDefault'
    console.log('onFormSubmit : ', this); 
   } */
   onFormSubmit = (event) => {
    event.preventDefault(); //What is 'preventDefault'
    console.log('onFormSubmit : ', this); 
   }
  /* this.state = { 
    done: false,
    items: []
};


axios.get(apiPool)
    .then(res => {
      const coins = res.data;
      console.log(coins);
      this.setState({ items: coins});
    }); */

  
  render() {
    var productos = new Array();
    var p,u=0;
   // console.log(this.state.prod[0].response.length);
    for (var i=0; i<this.state.prod[0].response.length; i++) {
      var hotel = this.state.prod[0].response[i]; // hasta aquí tienes cada hotel
      for (var detalle in hotel.products) {
      
       
        for (var dato in hotel.products[detalle]) {
        //   console.log (hotel.products[detalle][dato]); // Y aquí exploras cada dato del viaje
           productos[p,u]=hotel.products[detalle][dato]
          

         u++;
        }
        u=0;
        
      
      }
      p++;
    }
   // console.log(this.state.items[1].checkouts[0].id);
    //console.log(this.state.items[1].checkouts.length);
   // console.log(this.state.item[0].checkouts.length);
  // console.log(JSON.stringify(this.state.item));
    //console.log(JSON.stringify(this.state.item.response.products));
   // let hoteles = this.state.item.response.products
   // hoteles.map((hotel) => { console.log(hotel) })
   //console.log(this.state.prod.response);
  // console.log(this.state.items);
   var k,c=0;
   {this.state.prodto.map((ripleyo, index) => {
    
      try {
         console.log(ripleyo)
         
         //console.log(ripley[0].response[0].products.length)
      /*  console.log(ripley[0].response.length)
        console.log(ripley[0].response[0].products.length)
        console.log(ripley[0].response[1].products.length)
        console.log(ripley[0].response[2].products.length) */
        
      //  console.log(ripley[1].response.length)
   
           
           // for (var i=0;i<=2;i++){
         /*   productos.forEach( function(valor, j, array) {
              console.log("En el índice SM " + j + " hay este valor: " + valor);
            
          }); */
          //  }
          
           // }
      }
      catch(e)
      {
        
      }
    
      })}
  
  
  
   

    
   
  
    let value = 9;
    var style = {
      color: 'blue',
      fontSize: 10,
      maxlength:20,
      size:5,
     
    }
    var label={
      color: 'blue',
      fontSize: 10,
     
    }
    var table={
      color: 'blue',
      fontSize: 10,
      width:20,
     
    }
   /*  {this.state.items.map((ripley, index) => (
      console.log("abrir"),
      console.log(ripley[0]),
      <p key={index}>Hello!</p>
      
  ))} */
    return (
     
      <div className="content">
       
                  
        <Row>
        
          <Col xs={2} sm={6} md={6} lg={3}>
          <form onKeyUp={this.handleKeyUp} >
          <tbody>
          <table class="Table" style={{style}}>
          
        
          
        
        {
           
       
        
          
            this.state.items.map((ripley, index) => (
              
               Array.apply(0, Array(ripley.checkouts.length)).map(function (x, i) {
                
              return (
              
                <tr>
            <Card className="card-stats">
           
           <CardBody>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-left">
                     <form onSubmit={(e) => this.onFormSubmit(e)} className="ui form"> 
                    <td>
                       <tr>
                        <td>
                        < i className="nc-icon nc-money-coins text-success"   key={i} /></td>
                        <td><label style={{fontSize: 10,  color: 'blue'}}>Nro Caja: </label></td>
                        <td><input  id="cardInput{i}"  name='caja{i}' type="text" value={ripley.checkouts[i].id}  style={{fontSize: 14,  color: 'blue', size: 5, maxlength: 5}}/>
                        </td></tr>
                        <tr>
                        <td><label style={{fontSize: 10,  color: 'blue'}}>Productos: </label></td>
                        <td><input  id='cardInput{i}'  name='prod{i}' value={ripley.checkouts[i].products} type="text" style={{fontSize: 14,  color: 'blue', size: 5, maxlength: 5}}/>
                        </td></tr>
                        <div className="app">
                           <ul className="todo-list">
                                                        
                            {
                               
                                <Products prod={productos[0,1]} key={index}  />
                                
                            }
                             
                            
                             
                             
                               
                            
                                 
                               
                            
                              
                           
                             
                      
                               
                              </ul>
                            
                          </div>  


                        <tr>
                        <td><label style={{fontSize: 10,  color: 'blue'}}>Paso Caja: </label></td>
                        <td><input  id="cardInpu{i}"  name='pc{i}'  value={ripley.checkouts[i].costumers} type="text" style={{fontSize: 14,  color: 'blue', size: 5, maxlength: 5}}/></td>
                        </tr>
                        <td><label style={{fontSize: 10,  color: 'blue'}}>Venta: </label></td>
                        <td><input  id='cardInput{i}'  name='venta{i}' value={ripley.checkouts[i].daySales} type="text" style={{fontSize: 14,  color: 'blue', size: 5, maxlength: 5}}/>
                        
                        </td>
                        <td><input  id='cardInput{i}'  name='venta{i}' value={Date.now()+parseInt(Math.random(500))} type="text" style={{fontSize: 14,  color: 'blue', size: 5, maxlength: 5}}/>
                        
                        </td>
                        </td>   
                        </form>
                   </div>
             </Col>
            
                
              
              </CardBody>
            
               
              <CardFooter>
                <hr />
                <Stats>
                  {[
                    {
                      i: "far fa-calendar",
                      t: "Last day"
                    }
                  ]}
                </Stats>
              </CardFooter>
             </Card>
             
                 </tr>
              
             
              
              );
              
            })
          
         
    ))
          }
          
            </table>
            
            </tbody>
            
            <input type="submit" value="Submit" />
            
            </form>
            
          </Col>
        
          
        </Row>
      
        <Row>
          <Col xs={12} sm={12} md={4}>
            <Card>
              <CardHeader>
                <CardTitle>Email Statistics</CardTitle>
                <p className="card-category">Last Campaign Performance</p>
              </CardHeader>
              <CardBody>
                <Pie
                  data={dashboardEmailStatisticsChart.data}
                  options={dashboardEmailStatisticsChart.options}
                />
              </CardBody>
              <CardFooter>
                <div className="legend">
                  <i className="fa fa-circle text-primary" /> Opened{" "}
                  <i className="fa fa-circle text-warning" /> Read{" "}
                  <i className="fa fa-circle text-danger" /> Deleted{" "}
                  <i className="fa fa-circle text-gray" /> Unopened
                </div>
                <hr />
                <Stats>
                  {[
                    {
                      i: "fas fa-calendar-alt",
                      t: " Number of emails sent"
                    }
                  ]}
                </Stats>
              </CardFooter>
            </Card>
          </Col>
          <Col xs={12} sm={12} md={8}>
            <Card className="card-chart">
              <CardHeader>
                <CardTitle>NASDAQ: AAPL</CardTitle>
                <p className="card-category">Line Chart With Points</p>
              </CardHeader>
              <CardBody>
                <Line
                  data={dashboardNASDAQChart.data}
                  options={dashboardNASDAQChart.options}
                  width={400}
                  height={100}
                />
              </CardBody>
              <CardFooter>
                <div className="chart-legend">
                  <i className="fa fa-circle text-info" /> Tesla Model S{" "}
                  <i className="fa fa-circle text-warning" /> BMW 5 Series
                </div>
                <hr />
                <Stats>
                  {[
                    {
                      i: "fas fa-check",
                      t: " Data information certified"
                    }
                  ]}
                </Stats>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
      
    );
  }
  
}

export default Dashboard;
